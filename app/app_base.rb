# frozen_string_literal: true

require 'English'
require_relative 'silently'
require 'sinatra/base'
silently { require 'sinatra/contrib' } # N x "warning: method redefined"
require_relative 'http_json_hash/service'
require_relative 'json_hash_parse_helper'
require 'json'
require 'sprockets'
require 'uglifier'

class AppBase < Sinatra::Base
  def initialize(externals)
    @externals = externals
    super(nil)
  end

  silently { register Sinatra::Contrib }
  set :port, ENV['PORT']
  set :environment, Sprockets::Environment.new

  environment.append_path('/app/assets/stylesheets')
  environment.css_compressor = :sassc

  get '/assets/app.css', provides: [:css] do
    respond_to do |format|
      format.css do
        env['PATH_INFO'].sub!('/assets', '')
        settings.environment.call(env)
      end
    end
  end

  environment.append_path('/app/assets/javascripts')
  environment.js_compressor = Uglifier.new(harmony: true)

  get '/assets/app.js', provides: [:js] do
    respond_to do |format|
      format.js do
        env['PATH_INFO'].sub!('/assets', '')
        settings.environment.call(env)
      end
    end
  end

  def self.get_delegate(klass, name)
    get "/#{name}", provides: [:json] do
      respond_to do |format|
        format.json do
          target = klass.new(@externals)
          result = target.public_send(name, params)
          json({ name => result })
        end
      end
    end
  end

  private_class_method :get_delegate

  private

  def json_args
    @json_args ||= symbolized(json_payload)
  end

  def symbolized(hash)
    # named-args require symbolization
    hash.transform_keys(&:to_sym)
  end

  def json_payload
    request.body.rewind  # Already been read in sinatra 4.0.0 !
    json_hash_parse(request.body.read)
  end

  include JsonHashParseHelper

  set :show_exceptions, false

  error do
    error = $ERROR_INFO
    info = {
      exception: {
        request: {
          path: request.path,
          body: request.body ? request.body.read : nil
        },
        backtrace: error.backtrace
      }
    }
    exception = info[:exception]
    if error.instance_of?(::HttpJsonHash::ServiceError)
      exception[:http_service] = {
        path: error.path,
        args: error.args,
        name: error.name,
        body: error.body,
        message: error.message
      }
    else
      exception[:message] = error.message
    end
    puts JSON.pretty_generate(info)
    halt erb :error
  end
end
